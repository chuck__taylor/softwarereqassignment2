var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var studentSchema = new Schema({
    number: Number,
    firstName: String,
    lastName: String,
    gender: Number,
    DOB: String,
    residency: Number
}, { collection : 'students' });

var Students = mongoose.model('student', studentSchema);
    
mongoose.connect('mongodb://localhost:27017/assignment2');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    exports.Students = Students;
});