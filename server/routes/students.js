var express = require('express');
var router = express.Router();
var models = require('../app/models/student');
var bodyParser = require('body-parser');
var parseUrlencoded = bodyParser.urlencoded({extended: false});
var parseJSON = bodyParser.json();

/**router.route('/')
    .get(parseUrlencoded, parseJSON, function (request, response) {
            models.Students.find(function (error, students) {
                if (error) response.send(error);
                response.json({message: 'hello world'});
            });
    }); */

router.get('/', function(req, res) {
    res.jsonp({ message: 'test GET successful' });
});

module.exports = router;