
var express = require('express');
var logger = require('./logger');
var app = express();
var students = require('./routes/students');

var port = 8082; // process.env.PORT ||

app.use(function (request, response, next) {
    // response.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    response.header('Access-Control-Allow-Methods', 'POST, PATCH, GET, PUT, DELETE, OPTIONS');
    next();
});

app.use(logger);


app.use('/students', students);

app.listen(port, function () {
    console.log('Listening on port ' + port);
});
